// unzipPipe - unzippes a zip archive into a pipe
package main

import (
	"archive/zip"
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

func init() {
}

func main() {
	flag.Parse()
	var dataBuf bytes.Buffer
	dataW := bufio.NewWriter(&dataBuf)

	fileName := flag.Arg(0)
	// Open a zip archive for reading.
	r, err := zip.OpenReader(fileName)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(os.Stderr, "%s\n", fileName)
	defer r.Close()

	for _, f := range r.File {
		//fmt.Fprintf(os.Stderr, "Contents of %s:\n", f.Name)
		rc, err := f.Open()
		if err != nil {
			log.Fatal(err)
		}
		_, err = io.Copy(dataW, rc)
		if err != nil {
			log.Fatal(err)
		}
		rc.Close()
		dataW.Flush()
		fmt.Printf("%s\n", dataBuf.Bytes())
		//fmt.Fprintln(os.Stderr, "========================================")
		// reset buffers
		dataBuf.Reset()
	}
}
