// printableChar a POC on filter printable chars from random byte array
package main

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
)

func main() {
	buf := make([]byte, 256)
	rand.Read(buf)
	fmt.Printf("Random Input:\n")
	fmt.Printf("=============\n")
	fmt.Printf("%s", hex.Dump(buf))
	fmt.Printf("=============\n")
	err := ioutil.WriteFile("rand.bin", buf, 0700)
	if err != nil {
		log.Printf("%s\n", err.Error())
	} else {
		log.Printf("file `rand.bin` has been written\n")
	}
	out := cutNonPrintable(buf)
	fmt.Printf("Cut NonPrintable:\n")
	fmt.Printf("=================\n")
	fmt.Printf("%s", hex.Dump(out))
	fmt.Printf("=================\n")

	out = cutNonAscii(buf)
	fmt.Printf("Cut NonAscii:\n")
	fmt.Printf("=============\n")
	fmt.Printf("%s", hex.Dump(out))
	fmt.Printf("=============\n")
}

// filterByRegex filtert alle nicht druckbaren Zeichen aus einem byte array
func cutNonPrintable(in []byte) (out []byte) {
	re := regexp.MustCompile(`[^[:print:]]`)
	out = re.ReplaceAll(in, []byte(""))
	return out
}

func cutNonAscii(in []byte) (out []byte) {
	re := regexp.MustCompile(`[^[:ascii:]]`)
	out = re.ReplaceAll(in, []byte(""))
	return out
}
