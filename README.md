## About

This project was inspired by [a tweet from Halvar Flake](https://twitter.com/halvarflake/status/781821842835243008) 
about how to read a docx file for paranoid people.

<blockquote class="twitter-tweet" data-lang="de"><p lang="en" dir="ltr">Reading .docx files for the truly paranoid: unzip -p ./foo.docx | sed -e &#39;s/&lt;[^&gt;]\{1,\}&gt;//g; s/[^[:print:]]\{1,\}//g&#39;</p>&mdash; halvarflake (@halvarflake) <a href="https://twitter.com/halvarflake/status/781821842835243008?ref_src=twsrc%5Etfw">30. September 2016</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I have turned this into a go program.

## Compile

```
go build ./
```

## Install 

```
go install ./
```

## Usage

```
Usage of paranoiddocx:

paranoiddocx </path/to/filename.docx>

Parameters:
-----------

  -noFilter
    	if true document will not be filtered
```

## Known limitations

This program does not work with docx files that are written in languages with non ascii character set, like Kanji or Chineese for example.
