package main

import (
	"archive/zip"
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
)

var noFilter bool

func init() {
	flag.BoolVar(&noFilter, "noFilter", false, "if true document will not be filtered")
}

func main() {
	flag.Parse()
	var dataBuf bytes.Buffer
	dataW := bufio.NewWriter(&dataBuf)

	fileName := flag.Arg(0)
	// Check if it is a normal old doc file
	if IsDocfile(fileName) {
		fmt.Printf("This is a old style docfile, not a docx, aborting\n")
		os.Exit(99)
	}
	// Open a zip archive for reading.
	r, err := zip.OpenReader(fileName)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(os.Stderr, "%s\n", fileName)
	defer r.Close()

	for _, f := range r.File {
		//fmt.Fprintf(os.Stderr, "Contents of %s:\n", f.Name)
		rc, err := f.Open()
		if err != nil {
			log.Fatal(err)
		}
		_, err = io.Copy(dataW, rc)
		if err != nil {
			log.Fatal(err)
		}
		rc.Close()
		dataW.Flush()
		if noFilter == true {
			fmt.Printf("%s\n", dataBuf.Bytes())
			// reset buffers
			dataBuf.Reset()
			continue
		}
		// filter out tags
		tagRe := regexp.MustCompile(`(?m)<[^>]{1,}>`)
		result := tagRe.ReplaceAll(dataBuf.Bytes(), []byte(""))
		result = cutNonPrintable(result)
		// print result
		if result != nil {
			fmt.Printf("%s\n", result)
		}
		//fmt.Fprintln(os.Stderr, "========================================")
		// reset buffers
		dataBuf.Reset()
	}
}

// cutNonPrintable - cuts non printable characters from the input.
// returns the cut input.
// It basically filters out the non-printable character from the input.
func cutNonPrintable(in []byte) (out []byte) {
	var result bytes.Buffer
	w := bufio.NewWriter(&result)
	re := regexp.MustCompile(`(?m)[^[:print:]]{1,}`)
	finds := re.ReplaceAll(in, []byte(""))
	if finds != nil {
		w.Write(finds)
	}
	w.Flush()
	return result.Bytes()
}

// IsDocFile - takes a filename as argument.
// returns true is the file is a traditional DOC file for MS Office.
// Those files always start with the 4 magic byte of "D0 CF 11 E0".
// This function checks the first 4 bytes of the given file to be the
// right magic bytes.
func IsDocfile(fileName string) bool {
	magic := []byte("\xd0\xcf\x11\xe0")
	buf := make([]byte, 4)
	f, _ := os.Open(fileName)
	f.Read(buf)
	f.Close()
	if bytes.Equal(magic, buf) {
		return true
	}
	return false
}
